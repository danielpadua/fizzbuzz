package com.itau.fizzbuzz;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test
	public void testaFizzBuzz3() {
		String ret = FizzBuzz.fizzBuzz(3);
		assertEquals(ret, "Fizz");
	}

	@Test
	public void testaFizzBuzz5() {
		String ret = FizzBuzz.fizzBuzz(5);
		assertEquals(ret, "Buzz");
	}

	@Test
	public void testaFizzBuzz() {
		String ret = FizzBuzz.fizzBuzz(3);
		assertEquals(ret, "Fizz");
	}

	@Test
	public void testaFizzBuzz15() {
		String ret = FizzBuzz.fizzBuzz(15);
		assertEquals(ret, "FizzBuzz");
	}

	@Test
	public void testaFizzBuzzMulti3() {
		String ret = FizzBuzz.fizzBuzz(6);
		assertEquals(ret, "Fizz");
	}

	@Test
	public void testaFizzBuzzMulti5() {
		String ret = FizzBuzz.fizzBuzz(10);
		assertEquals(ret, "Buzz");
	}

	@Test
	public void testaFizzBuzzMulti15() {
		String ret = FizzBuzz.fizzBuzz(30);
		assertEquals(ret, "FizzBuzz");
	}

	@Test
	public void testaFizzBuzzNada() {
		String ret = FizzBuzz.fizzBuzz(17);
		assertEquals(ret, "17");
	}
	
	@Test
	public void testaFizzBuzzIterativo() {
		String ret = FizzBuzz.fizzBuzzIterativo(5);
		String res = "1 2 Fizz 4 Buzz";
		assertEquals(ret, res);
	}

}
