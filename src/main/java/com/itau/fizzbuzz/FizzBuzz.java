package com.itau.fizzbuzz;

public class FizzBuzz {

	public static String fizzBuzz(int i) {
		if ((i % 3 == 0) && (i % 5 == 0)) {
			return "FizzBuzz";
		}

		if (i % 5 == 0) {
			return "Buzz";
		}

		if (i % 3 == 0) {
			return "Fizz";
		}

		return String.valueOf(i);
	}

	public static String fizzBuzzIterativo(int i) {
		StringBuilder sb = new StringBuilder();
		for (int j = 1; j <= i; j++) {
			if (j < i) {
				sb.append(fizzBuzz(j) + " ");
			} else {
				sb.append(fizzBuzz(j));
			}			
		}
		return sb.toString();
	}

}
